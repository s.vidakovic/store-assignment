package Program;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class Appliances extends Product{

	//fields
	private String model;
	private String production_date;
	private double weight;
	
	//constructors
	public Appliances() {
		super();
		model = "";
		production_date = "";
		weight = 0.0;
	}
	
	public Appliances(String name, String brand, double price, String model, String production_date, double weight) {
		super(name, brand, price);
		this.model = model;
		this.production_date = production_date;
		this.weight = weight;
	}

	//methods
	@Override
	public double productPrice(double price) {
		
		LocalDate date = LocalDate.now();
		
		if(price > 999 && (date.getDayOfWeek().equals(DayOfWeek.SUNDAY) || date.getDayOfWeek().equals(DayOfWeek.SATURDAY))) {
			price = price * 0.93;
		}
		return price;
	}

	
	//getters and setters
	public String getModel() {
		return model;
	}

	public String getProduction_date() {
		return production_date;
	}

	public double getWeight() {
		return weight;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public void setProduction_date(String production_date) {
		this.production_date = production_date;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}			
	
	@Override
	public String toString() {
		return super.toString() + "\tModel: " + getModel();
	}
}
