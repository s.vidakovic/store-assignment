package Program;

import java.time.DayOfWeek;
import java.time.LocalDate;

public class Clothes extends Product{
	
	//fields
	private String size;
	private String color;
	
	//constructors
	public Clothes() {
		super();
		size = "";
		color = "";
	}
	
	public Clothes(String name, String brand, double price, String size, String color) {
		super(name, brand, price);
		this.size = size;
		this.color = color;
	}

	//mathods
	@Override
	public double productPrice(double price) {
		
		LocalDate date = LocalDate.now();
		
		if(!(date.getDayOfWeek().equals(DayOfWeek.SATURDAY) || date.getDayOfWeek().equals(DayOfWeek.MONDAY))) {
			price = price * 0.90;
		}
		return price;
	}
	
	@Override
	public String toString() {
		return super.toString() + "\t" + getSize() + "\t" + getColor();
	}
	
	//getters and setters
	public String getSize() {
		return size;
	}

	public String getColor() {
		return color;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public void setColor(String color) {
		this.color = color;
	}	
	
	
}
