package Program;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class FoodAndBeverages extends Product {

	//fields
	private String expiration_date;
	
	//constructors
	public FoodAndBeverages() {
		super();
		expiration_date = "";
	}
	
	public FoodAndBeverages(String name, String brand, double price, String expiration_date) {
		super(name, brand, price);
		this.expiration_date = expiration_date;
	}
	
	//methods
	@Override
	public double productPrice(double price) {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate now = LocalDate.now();
		LocalDate nowPlus5 = now.plusDays(5);
		LocalDate e_date = LocalDate.parse(expiration_date, dtf);
		
		if(e_date.isAfter(nowPlus5) || e_date.isEqual(nowPlus5)) {
			price = price * 0.70;
		}else if(e_date.isEqual(now)) {
			price = price * 0.30;
		}
		return price;
	}
	
	
	//getters and setters
	public String getExpiration_date() {
		return expiration_date;
	}

	public void setExpiration_date(String expiration_date) {
		this.expiration_date = expiration_date;
	}
	
	
	
	
}
