package Program;


public class Product {

	//fields
	private String name;
	private String brand;
	private double price;
	
	//constructors
	public Product(){
		name = "";
		brand = "";
		price = 0.0;
	}
	

	public Product(String name, String brand, double price) {
		super();
		this.name = name;
		this.brand = brand;
		this.price = price;
	}

	//getters and setters
	public String getName() {
		return name;
	}
	
	public String getBrand() {
		return brand;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public void setPrice(float price) {
		this.price = price;
	}

	//methods
	public double productPrice(double price) {
		return price;
	}
		
	public String toString() {
		return name + "\t" + brand;
	}	 
}
